from bottle import route, run, request
from petl import fromcsv, look
from bottle import response


@route('/getlocation')
def get_locationid():
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Content-type'] = 'application/json'

    location_id = request.query.id

    locationscsv = fromcsv('store_locations.csv')

    selected_location = locationscsv[int(location_id)]

    selected_storename = selected_location[1]
    selected_latitude = selected_location[4]
    selected_longitude = selected_location[5]

    d = {"storename":selected_storename,"storelat":selected_latitude,"storelong":selected_longitude}

    return d

run(host='localhost', port=8080, debug=True)
