from xml.dom import minidom, Node
import sys

def scanNode(node, file=sys.stdout, level = 0):
    if node.hasChildNodes():
        for child in node.childNodes:
                scanNode(child, file, level + 1)
    else:
        if node.nodeValue.strip() != "":
            file.write(node.nodeValue + ',')

    if node.nodeType == Node.ELEMENT_NODE:
        if node.nodeName == "Postcode":
            nodePostCode = node.childNodes[0].nodeValue
            info = getLocation(nodePostCode)
            file.write(info)

    if node.nodeType == Node.ELEMENT_NODE:
        if node.hasAttributes():
            file.write(node.attributes.item(0).nodeValue + ',')
        if level == 2:
            file.write("\n")

def getLocation(currentNode):
    import csv
    with open('locations.csv', newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            postcode = row['Postcode']
            if (int(postcode) == int(currentNode)):
                return str(row ['Suburb'] + ',' + row['Lat'] + ',' + row['Lon'] + ',')

storesDOMTree = minidom.parse('stores.xml')

convergedFile = open('store_locations.csv', 'w')

convergedFile.write("id,name,postcode,suburb,latitude,longitude,state\n")
scanNode(storesDOMTree, convergedFile)
convergedFile.close()